from ultralytics import YOLO
import torch
import cv2
import os
from config import model_path, data_path, results_path, model_format
import time
import threading


# define cpu or gpu to use
DEVICE = torch.device("cuda")


# export the model in our desired format
def export(model, format, img_size=640, half=False, int8=False, workspace=4):
    model.export(format=format, imgsz=img_size, half=half, int8=int8, workspace=workspace)


# tracking algorithm
def run_tracker(filename, model, file_index, results_path):

    # define paths of result files
    os.makedirs(results_path, exist_ok=True)
    output_video_file = f"{results_path}/output_video_{file_index}.mp4"
    output_file = f"{results_path}/video_info_{file_index}.txt"

    # get the video to inference
    video = cv2.VideoCapture(filename) 

    # extract necessary information
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    frame_rate_original = int(video.get(cv2.CAP_PROP_FPS))
    frame_size = (int(video.get(3)), int(video.get(4)))

    # define the output video writer
    out = cv2.VideoWriter(output_video_file, fourcc, frame_rate_original, frame_size)

    # define calculation metrics
    inferencing_time = 0
    frame_count = 0
    video_proc_time = 0

    while True:

        # start video processing
        frame_start_time = cv2.getTickCount()

        ret, frame = video.read()

        if not ret:
            break

        # start inferencing
        start_time = cv2.getTickCount()

        results = model.track(frame, tracker="botsort.yaml", persist=True, conf=0.35, iou=0.70, half=False,
                              device=DEVICE)
        
        # end inferencing
        end_time = cv2.getTickCount()

        res_plotted = results[0].plot()

        out.write(res_plotted)

        # end video processing
        frame_end_time = cv2.getTickCount()
        frame_count += 1

        # calculation of time metrics
        processing_time = (end_time - start_time) / cv2.getTickFrequency()
        vid_processing_time =  (frame_end_time - frame_start_time) / cv2.getTickFrequency()
        
        inferencing_time += processing_time
        video_proc_time += vid_processing_time

        # to get number of channels (there should be a better way :D)
        num_channels = frame.shape[2]
        
    # calculate frame rate
    frame_rate = frame_count / video_proc_time

    # log results
    with open(output_file, "w") as file:        
        file.write(f"FPS: {frame_rate }\n")
        file.write(f"Resolution: {frame_size}\n")
        file.write(f"Channels: {num_channels}\n")
        file.write(f"Video Inference Time: {inferencing_time} seconds\n")
        file.write(f"Video Processing Time: {video_proc_time} seconds\n")        

    video.release()
    out.release()


def main():
    # get the yolov8 models
    model1 = YOLO(model_path)
    model2 = YOLO(model_path)
    model3 = YOLO(model_path)
    model4 = YOLO(model_path)

    # get the video files
    video_files = os.listdir(data_path)

    # Create the tracker threads
    tracker_thread1 = threading.Thread(target=run_tracker, args=(os.path.join(data_path, video_files[0]), model1, 1, results_path), daemon=True)
    tracker_thread2 = threading.Thread(target=run_tracker, args=(os.path.join(data_path, video_files[1]), model2, 2, results_path), daemon=True)
    tracker_thread3 = threading.Thread(target=run_tracker, args=(os.path.join(data_path, video_files[2]), model3, 3, results_path), daemon=True)
    tracker_thread4 = threading.Thread(target=run_tracker, args=(os.path.join(data_path, video_files[3]), model4, 4, results_path), daemon=True)

    # start calculating the total time
    start_time = time.time()

    # Start the tracker threads
    tracker_thread1.start()
    tracker_thread2.start()
    tracker_thread3.start()
    tracker_thread4.start()

    # Wait for the tracker threads to finish
    tracker_thread1.join()
    tracker_thread2.join()
    tracker_thread3.join()
    tracker_thread4.join()

    end_time = time.time()

    # calculation of total time
    elapsed_time = end_time - start_time
    total_time_file = f"{results_path}/total_time_info.txt"

    # log total time results
    with open(total_time_file, "w") as file: 
        file.write(f"Total elapsed time: {elapsed_time} seconds")

    # if we want to output in a desired format for different platforms
    if model_format:
        export(model1, model_format)

if __name__ == "__main__":
    main()